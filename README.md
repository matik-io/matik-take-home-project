# Matik Take Home Project


Thanks for taking the time to complete this take home project. This project consists of two parts:
1. A small coding project
2. Some follow up questions about the project

To submit the project, please make a single zip file containing a text file with your answers to the technical questions and a single folder containing the results of the coding test.
## Coding Test
Matik’s product is built around the automated creation of documents. For this coding test, we’d like you to build a small document automation service. In order to create the service, you will use the Google Slides API with credentials provided by Matik.

### Test Specification
For nearly every presentation that our customers give they create a title slide, which serves to introduce them to their customer and provide a bit of context for the rest of the presentation ([example](https://drive.google.com/open?id=1kRNimkADddnCpVkkI5PTS9Ji-d8-WOaMXqvYw7q3wcA)). We’d like you to build a web-based title slide generator that enables a user to automatically create a title slide in google slides with information that they enter. The user should be able to enter the following information that will be added to the title slide:
- Name of the user creating the title slide
- Job title of the user creating the title slide
- Name of the company the title slide is meant for
- Date of the presentation
- Logo of the company the title slide is meant for (you can use Clearbit to fetch the logo based on the url).

Given those inputs your program should create a net new title slide in Google Slides and save it to the google drive of the user filling out the form. In order to do this, you’ll need to set up OAuth authentication for the user. The OAuth credentials you need to set up the app are located [here](https://docs.google.com/document/d/1IyHTXIbM6eFfLeTxXsnDAo2ZH-eSyH9ly1Wd1glAWgQ/edit?usp=sharing). 
Note: The Google OAuth documentation leaves something to be desired, so we’ve created both a sample Python/Flask app and a sample HTML/JavaScript page that implements Google OAuth as a reference.
### Test Deliverables
We would like you to give us a web application that can be run locally and can compile and run in one step. The web application should consist of a single page containing a form where a user can enter the information described above. Upon form submission, you should display a link to the created Google Slides presentation.
### Platform Choice
You may use any framework or language you are comfortable with.
### Timing and Assessment
This test is meant to take around a half a day. We’re not looking for pixel-perfect design, just something that works well from a technical standpoint. If you have any questions feel free to reach out to zak@matik.io.
## Technical Questions
- Why did you choose the language and framework for this project? What are some of the tradeoffs for using this language/framework?
- How would you iterate on this project beyond what was initially required?
